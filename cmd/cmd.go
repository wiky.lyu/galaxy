/*
 * Copyright (C) 2018-2020 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */

package cmd

import (
	"context"
	"encoding/json"
	"time"

	"github.com/abiosoft/ishell"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"gitlab.com/wiky.lyu/galaxy/rpc"
	"gitlab.com/wiky.lyu/galaxy/rpc/pb"
	"gitlab.com/wiky.lyu/galaxy/tunnel"
)

var (
	VERSION = ishell.Cmd{
		Name:    "version",
		Aliases: []string{"v"},
		Help:    "version - show server version",
		Func:    version,
	}
	LIST = ishell.Cmd{
		Name:    "list",
		Aliases: []string{"ls"},
		Help:    "list -d [query] - list tunnels",
		Func:    list,
	}
	START = ishell.Cmd{
		Name: "start",
		Help: "start [tunnel] - start a tunnel",
		Func: start,
	}
	STOP = ishell.Cmd{
		Name: "stop",
		Help: "stop [tunnel] - stop a tunnel",
		Func: stop,
	}
	ADD = ishell.Cmd{
		Name: "add",
		Help: "add -n [name] -a [addr] -p [port] -i [input] -o [output] -m [maxConn] - add a new tunnel",
		Func: add,
	}
	UPDATE = ishell.Cmd{
		Name:    "update",
		Aliases: []string{"up"},
		Help:    "update -n [name] -a [addr] -p [port] -i [input] -o [output] -m [maxConn] - update a tunnel",
		Func:    update,
	}
	REMOVE = ishell.Cmd{
		Name:    "remove",
		Aliases: []string{"rm"},
		Help:    "remove [tunnel] - remove a tunnel",
		Func:    remove,
	}
	STAT = ishell.Cmd{
		Name:    "stat",
		Aliases: []string{"st"},
		Help:    "stat [tunnel] -f from -t to",
		Func:    stat,
	}
	LOAD = ishell.Cmd{
		Name: "load",
		Help: "load [file] - load tunnels from a config file",
		Func: load,
	}
)

func version(ctx *ishell.Context) {
	gc := ctx.Get("gc").(*rpc.GravityClient)
	if len(ctx.Args) != 0 {
		ctx.Println("invalid argument")
		return
	}
	reply, err := gc.Version(context.Background(), &pb.Void{})
	if err != nil {
		ctx.Err(err)
		return
	}
	ctx.Printf("Name   : %s\n", reply.AppName)
	ctx.Printf("Version: %s\n", reply.AppVersion)
	ctx.Printf("Build  : %s\n", reply.AppBuild)
	ctx.Printf("Commit : %s\n", reply.AppCommit)
}

func list(ctx *ishell.Context) {
	gc := ctx.Get("gc").(*rpc.GravityClient)

	var query string
	var details bool
	fs := pflag.FlagSet{}
	fs.BoolVarP(&details, "details", "d", false, "details")
	if err := fs.Parse(ctx.Args); err != nil {
		ctx.Err(err)
		return
	}
	query = fs.Arg(0)
	if query == "" {
		query = "*"
	}

	req := &pb.ListTunnelRequest{
		Query:   query,
		Options: details,
	}
	reply, err := gc.ListTunnel(context.Background(), req)
	if err != nil {
		ctx.Err(err)
		return
	}
	for _, t := range reply.Tunnels {
		ctx.Printf("%s - listen on %s:%d(%s/%s)\t%s\n", t.Name, t.Addr, t.Port, t.IProtocol, t.OProtocol, t.Status.String())
		if details {
			if len(t.IOptions) > 0 {
				ctx.Printf("    %s options:\n", t.IProtocol)
				for _, key := range SortMap(t.IOptions) {
					ctx.Printf("        %s:%s\n", key, t.IOptions[key])
				}
			} else {
				ctx.Printf("    %s no option\n", t.IProtocol)
			}
			if len(t.OOptions) > 0 {
				ctx.Printf("    %s options:\n", t.OProtocol)
				for _, key := range SortMap(t.OOptions) {
					ctx.Printf("        %s:%s\n", key, t.OOptions[key])
				}
			} else {
				ctx.Printf("    %s no option\n", t.OProtocol)
			}
		}
	}
}

func start(ctx *ishell.Context) {
	gc := ctx.Get("gc").(*rpc.GravityClient)
	for _, name := range ctx.Args {
		req := &pb.StartTunnelRequest{Name: name}
		reply, err := gc.StartTunnel(context.Background(), req)
		if err != nil {
			ctx.Err(err)
			return
		}
		ctx.Printf("%s:%s\n", name, tunnel.GetStatusName(reply.Status))
	}
}

func stop(ctx *ishell.Context) {
	gc := ctx.Get("gc").(*rpc.GravityClient)
	for _, name := range ctx.Args {
		req := &pb.StopTunnelRequest{Name: name}
		reply, err := gc.StopTunnel(context.Background(), req)
		if err != nil {
			ctx.Err(err)
			return
		}
		ctx.Printf("%s:%s\n", name, tunnel.GetStatusName(reply.Status))
	}
}

func add(ctx *ishell.Context) {
	gc := ctx.Get("gc").(*rpc.GravityClient)

	var name, addr, input, output string
	var port uint32
	var maxConn int32

	fs := pflag.FlagSet{}
	fs.StringVarP(&name, "name", "n", "", "name")
	fs.StringVarP(&addr, "addr", "a", "", "addr")
	fs.Uint32VarP(&port, "port", "p", 0, "port")
	fs.StringVarP(&input, "input", "i", "", "input")
	fs.StringVarP(&output, "output", "o", "", "output")
	fs.Int32VarP(&maxConn, "maxConn", "m", -1, "maxConn")
	if err := fs.Parse(ctx.Args); err != nil {
		ctx.Err(err)
		return
	}

	if name == "" || port == 0 || input == "" || output == "" {
		ctx.Println("invalid argument")
		return
	}

	req := &pb.AddTunnelRequest{
		Name:    name,
		Addr:    addr,
		Port:    port,
		Input:   input,
		Output:  output,
		MaxConn: maxConn,
	}
	reply, err := gc.AddTunnel(context.Background(), req)
	if err != nil {
		ctx.Err(err)
		return
	}
	ctx.Println(tunnel.GetStatusName(reply.Status))
}

func update(ctx *ishell.Context) {
	gc := ctx.Get("gc").(*rpc.GravityClient)

	var name, addr, input, output string
	var port uint32

	fs := pflag.FlagSet{}
	fs.StringVarP(&name, "name", "n", "", "name")
	fs.StringVarP(&addr, "addr", "a", "", "addr")
	fs.Uint32VarP(&port, "port", "p", 0, "port")
	fs.StringVarP(&input, "input", "i", "", "input")
	fs.StringVarP(&output, "output", "o", "", "output")
	if err := fs.Parse(ctx.Args); err != nil {
		ctx.Err(err)
		return
	}

	if name == "" {
		ctx.Println("invalid argument")
		return
	}

	req := &pb.UpdateTunnelRequest{
		Name:   name,
		Addr:   addr,
		Port:   port,
		Input:  input,
		Output: output,
	}
	reply, err := gc.UpdateTunnel(context.Background(), req)
	if err != nil {
		ctx.Err(err)
		return
	}
	ctx.Println(tunnel.GetStatusName(reply.Status))
}

func remove(ctx *ishell.Context) {
	gc := ctx.Get("gc").(*rpc.GravityClient)

	for _, name := range ctx.Args {
		req := &pb.RemoveTunnelRequest{
			Name: name,
		}
		reply, err := gc.RemoveTunnel(context.Background(), req)
		if err != nil {
			ctx.Err(err)
			return
		}
		ctx.Printf("%s:%s\n", name, tunnel.GetStatusName(reply.Status))
	}
}

func stat(ctx *ishell.Context) {
	gc := ctx.Get("gc").(*rpc.GravityClient)

	var fromStr, toStr string
	var day bool
	fs := pflag.FlagSet{}
	fs.StringVarP(&fromStr, "from", "f", "", "from time")
	fs.StringVarP(&toStr, "to", "t", "", "to time")
	fs.BoolVarP(&day, "day", "d", false, "day")
	if err := fs.Parse(ctx.Args); err != nil {
		ctx.Err(err)
		return
	}
	name := fs.Arg(0)
	if name == "" {
		ctx.Println("invalid argument")
		return
	}
	from, err := ExtractIntSlice(fromStr)
	if err != nil {
		ctx.Err(err)
		return
	}
	to, err := ExtractIntSlice(toStr)
	if err != nil {
		ctx.Err(err)
		return
	}

	now := time.Now().Add(time.Hour)
	fromTime := time.Date(GetIntSliceDefault(from, 0, 1), time.Month(GetIntSliceDefault(from, 1, 1)), GetIntSliceDefault(from, 2, 1), GetIntSliceDefault(from, 3, 0), 0, 0, 0, time.Local)
	toTime := time.Date(GetIntSliceDefault(to, 0, now.Year()), time.Month(GetIntSliceDefault(to, 1, int(now.Month()))), GetIntSliceDefault(to, 2, now.Day()), GetIntSliceDefault(to, 3, now.Hour()), 0, 0, 0, time.Local)

	req := &pb.StatTunnelRequest{
		Name: name,
		From: fromTime.Format(time.RFC3339),
		To:   toTime.Format(time.RFC3339),
	}
	reply, err := gc.StatTunnel(context.Background(), req)
	if err != nil {
		ctx.Err(err)
		return
	}

	if reply.Status != tunnel.StatusOK {
		ctx.Printf("%s:%s\n", name, tunnel.GetStatusName(reply.Status))
		return
	}

	if day {
		var cy, cm, cd uint32
		var i, e uint64
		for _, fs := range reply.FlowStats {
			if cy == fs.Year && cm == fs.Month && cd == fs.Day {
				i += fs.Ingress
				e += fs.Egress
			} else {
				if cy != 0 {
					ctx.Printf("%04d-%02d-%02d - %s/%s\n", cy, cm, cd, FormatBytes(i), FormatBytes(e))
				}
				cy = fs.Year
				cm = fs.Month
				cd = fs.Day
				i = fs.Ingress
				e = fs.Egress
			}
		}
		if cy != 0 {
			ctx.Printf("%04d-%02d-%02d - %s/%s\n", cy, cm, cd, FormatBytes(i), FormatBytes(e))
		}
	} else {
		for _, fs := range reply.FlowStats {
			ctx.Printf("%04d-%02d-%02d %02d:00:00 - %s/%s\n", fs.Year, fs.Month, fs.Day, fs.Hour, FormatBytes(fs.Ingress), FormatBytes(fs.Egress))
		}
	}

}

func load(ctx *ishell.Context) {
	gc := ctx.Get("gc").(*rpc.GravityClient)
	if len(ctx.Args) != 1 {
		ctx.Println("invalid argument")
		return
	}
	name := ctx.Args[0]
	v := viper.New()
	v.SetConfigFile(name)
	if err := v.ReadInConfig(); err != nil {
		ctx.Err(err)
		return
	}

	cfgs := make([]*TunnelConfig, 0)
	if err := v.UnmarshalKey("tunnels", &cfgs); err != nil {
		ctx.Err(err)
		return
	}
	for _, cfg := range cfgs {
		input, _ := json.Marshal(cfg.Input)
		output, _ := json.Marshal(cfg.Output)
		name := cfg.Name
		req := &pb.AddTunnelRequest{
			Name:    name,
			Addr:    cfg.Addr,
			Port:    uint32(cfg.Port),
			Input:   string(input),
			Output:  string(output),
			MaxConn: cfg.MaxConn,
		}
		reply, err := gc.AddTunnel(context.Background(), req)
		if err != nil {
			ctx.Err(err)
			break
		}
		if reply.Status == tunnel.StatusOK {
			if cfg.AutoStart {
				req := &pb.StartTunnelRequest{
					Name: name,
				}
				reply, err := gc.StartTunnel(context.Background(), req)
				if err != nil {
					ctx.Err(err)
					break
				}
				ctx.Printf("%s:%s\n", name, tunnel.GetStatusName(reply.Status))
			} else {
				ctx.Printf("%s:OK\n", name)
			}
		} else {
			ctx.Printf("%s:%s\n", name, tunnel.GetStatusName(reply.Status))
		}
	}
}
