/*
 * Copyright (C) 2015-2020 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */
package cipher

import (
	"crypto/sha1"
	"golang.org/x/crypto/hkdf"
	"io"
)

func HKDF_SHA1(key []byte, size int, salt, info []byte) []byte {
	r := hkdf.New(sha1.New, key, salt, info)
	subkey := make([]byte, size)
	io.ReadFull(r, subkey)
	return subkey
}
