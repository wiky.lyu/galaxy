/*
 * Copyright (C) 2018-2020 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */

package config

import (
	"github.com/spf13/viper"
)

var (
	gravityViper *viper.Viper
	tunnelsViper *viper.Viper
)

func init() {
	gravityViper = viper.New()
	tunnelsViper = viper.New()
}

func Read(c1, c2 string) error {
	gravityViper.SetConfigFile(c1)
	if err := gravityViper.ReadInConfig(); err != nil {
		return err
	}
	tunnelsViper.SetConfigFile(c2)
	if err := tunnelsViper.ReadInConfig(); err != nil {
		return err
	}
	return nil
}

func GetGravityConfig() *GalaxyConfig {
	var cfg GalaxyConfig
	gravityViper.SetDefault("galaxy.tls.enabled", true)
	gravityViper.UnmarshalKey("galaxy", &cfg)

	return &cfg
}

func GetTunnelsConfig() []*TunnelConfig {
	tcfgs := make([]*TunnelConfig, 0)
	tunnelsViper.UnmarshalKey("tunnels", &tcfgs)
	return tcfgs
}
