/*
 * Copyright (C) 2018-2020 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */

package rpc

import (
	"context"
	"fmt"

	"gitlab.com/wiky.lyu/galaxy/config"
	"gitlab.com/wiky.lyu/galaxy/rpc/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/reflection"
	"log"
	"net"
)

func NewGRPCServer(tlsEnabled bool, tlsPem, tlsKey, username, password string) *grpc.Server {
	opts := make([]grpc.ServerOption, 0)
	if tlsEnabled {
		creds, err := credentials.NewServerTLSFromFile(tlsPem, tlsKey)
		if err != nil {
			log.Fatalf("NewServerTLSFromFile(%s,%s) failed:%v", tlsPem, tlsKey, err)
		}
		opts = append(opts, grpc.Creds(creds))
		log.Printf("tls enabled\n")
	}
	if username != "" && password != "" {
		ac := &authInfo{
			Username: username,
			Password: password,
		}
		opts = append(opts, grpc.StreamInterceptor(ac.streamInterceptor))
		opts = append(opts, grpc.UnaryInterceptor(ac.unaryInterceptor))
		log.Printf("auth enabled\n")
	}
	return grpc.NewServer(opts...)
}

func Serve(appName, appVersion, appBuild, appCommit string) error {
	cfg := config.GetGravityConfig()
	tcfgs := config.GetTunnelsConfig()

	addr := cfg.Addr
	username := cfg.Username
	password := cfg.Password
	tlsEnabled := cfg.TLS.Enabled
	tlsPem := cfg.TLS.Pem
	tlsKey := cfg.TLS.Key
	lis, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}

	s := NewGRPCServer(tlsEnabled, tlsPem, tlsKey, username, password)

	gs := NewGravityServer(appName, appVersion, appBuild, appCommit)
	pb.RegisterGravityServer(s, gs)
	reflection.Register(s)

	/* 载入配置文件中定义的代理 */
	for _, tcfg := range tcfgs {
		if err := gs.addTunnel(tcfg.Name, tcfg.Addr, tcfg.Port, tcfg.Input, tcfg.Output, tcfg.MaxConn); err != nil {
			log.Printf("**ERROR** add tunnel %s failed:%v", tcfg.Name, err)
			continue
		}
		log.Printf("add tunnel %s successfully", tcfg.Name)
		if tcfg.AutoStart {
			if err := gs.startTunnel(tcfg.Name); err != nil {
				log.Printf("**ERROR** start tunnel %s failed:%v", tcfg.Name, err)
			} else {
				log.Printf("start tunnel %s successfully", tcfg.Name)
			}
		}
	}

	log.Printf("listen on %s\n", addr)
	go func() {
		if err := s.Serve(lis); err != nil {
			panic(err)
		}
	}()
	return nil
}

type authInfo struct {
	Username, Password string
}

func (ac *authInfo) streamInterceptor(srv interface{}, stream grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
	if err := ac.authorize(stream.Context()); err != nil {
		return err
	}

	return handler(srv, stream)
}

func (ac *authInfo) unaryInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	if err := ac.authorize(ctx); err != nil {
		return nil, err
	}

	return handler(ctx, req)
}

func (ac *authInfo) authorize(ctx context.Context) error {
	if ac.Username == "" || ac.Password == "" {
		return nil
	}
	if md, ok := metadata.FromIncomingContext(ctx); ok {
		if len(md["username"]) > 0 && md["username"][0] == ac.Username &&
			len(md["password"]) > 0 && md["password"][0] == ac.Password {
			return nil
		}

		return fmt.Errorf("Invalid Authorization")
	}

	return fmt.Errorf("Empty Authorization")
}
