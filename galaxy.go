/*
 * Copyright (C) 2018-2020 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */

package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/spf13/pflag"
	"gitlab.com/wiky.lyu/galaxy/config"
	"gitlab.com/wiky.lyu/galaxy/rpc"
)

var (
	AppName    = "galaxy"
	AppVersion = "1.0.0"
	AppBuild   string
	AppCommit  string
)

const (
	openFileLimit = 1024 * 1024
)

func init() {
	var c1, c2 string
	pflag.StringVarP(&c1, "config", "c", "/etc/galaxy.yaml", "config file")
	pflag.StringVarP(&c2, "tunnels", "t", "/etc/tunnels.yaml", "tunnels file")
	pflag.Parse()
	if err := config.Read(c1, c2); err != nil {
		panic(err)
	}

	setup()
}

func setup() {
	rLimit := syscall.Rlimit{
		Max: openFileLimit,
		Cur: openFileLimit,
	}
	syscall.Setrlimit(syscall.RLIMIT_NOFILE, &rLimit)

	if err := syscall.Getrlimit(syscall.RLIMIT_NOFILE, &rLimit); err != nil {
		log.Printf("**failed to getrlimit**")
		return
	}
	log.Printf("rlimit.max=%d, rlimit.cur=%d", rLimit.Max, rLimit.Cur)
	if rLimit.Max < openFileLimit || rLimit.Cur < openFileLimit {
		log.Printf("**ATTENTION** too less open file limit. Run this program as root to set rlimit")
	}
}

func main() {
	if err := rpc.Serve(AppName, AppVersion, AppBuild, AppCommit); err != nil {
		panic(err)
	}

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, os.Interrupt, os.Kill)
	<-sigs
}
