## Galaxy

使用[GO](https://golang.org/)实现的网络代理管理程序。支持自定义的***输入(input)***和***输出(output)***协议。

Galaxy的前身是[Skywalker](https://gitlab.com/wiky.lyu/skywalker)。

### PREREQUISITES

* GO1.9

### 简介

galaxy可以以C/S模式工作，gravity作为其客户端通过命令执行特定的功能。比如增加代理、启动代理、关闭代理、查看代理信息等。

建议使用systemd或者supervisord启动galaxy，然后使用gravity对其进行管理。

### 编译

* `make`

将在**bin**目录下生成两个可执行文件，gravity和galaxy。

### 安装

* `make install`

默认安装在/usr/local目录下，如果要调整安装目录，需要手动修改Makefile里的*INSTALLDIR*变量。同时会自动生成galaxy的配置文件、TLS密钥和systemd脚本。

### 运行

安装后可使用systemd启动galaxy。

`sudo systemctl start galaxy`

因为默认的配置使用的是默认端口12121，且没有设置RPC密码，因此可以直接执行gravity。

`gravity`

如果设置了用户密码，则使用-u和-p参数启动gravity。

`gravity -u test -p 123`

启动gravity后便可以以交互方式对galaxy进行操作。

### 配置

配置文件支持[JSON](https://www.json.org/)和[YAML](http://yaml.org/)格式，默认使用[YAML](http://yaml.org/)。

RPC配置如下：
```
gravity:
  addr: 0.0.0.0:12121   # RPC监听地址
  username: abc         # RPC用户名
  password: 123         # RPC密码
  tls:
    enabled: true       # 开启TLS
    pem: /usr/local/etc/galaxy/certs/server.pem # 开启TLS时必须指定对应的TLS密钥
    key: /usr/local/etc/galaxy/certs/server.key
```

同时，可以直接在配置文件里指定初始载入的代理。如下：

```
tunnels:
  - name: h1            # 每个代理的名字必须唯一
    addr: 127.0.0.1     # 监听端口
    port: 8888          # 监听端口
    autoStart: true     # 是否载入的时候自动启动
    input:              # input配置
      protocol: http    # 每个input或者output必须指定protocol类型
    output:
      protocol: shadowsocks
      serverAddr: hk-1.v2speed.net      # shadowsocks额外的配置
      serverPort: 31108
      method: aes-256-cfb
      password: Mm8NNPf8lp
  - name: l1
    addr: 127.0.0.1
    port: 11111
    autoStart: true
    input:
      protocol: socks5
    output:
      protocol: shadowsocks
      serverAddr: 127.0.0.1
      serverPort: 22222
      method: aes-256-gcm
      password: 123456
```

### 支持的协议

当前支持的代理协议如下

#### 入口协议

* HTTP(s)
* Shadowsocks
* Socks5
* Gastly

#### 出口协议

* Shadowsocks
* Socks5
* Direct
* Gastly


### Gravity命令

* ***version***

  查看当前galaxy版本

* ***list***

  获取当前代理列表，可以通过参数**-o**查看代理详情，同时支持模糊搜索

  `list -o r*`

* ***load***

  从文件当中载入代理配置，文件格式可以是JSON或者YAML

  `load ./config/tunnels.yaml`

* ***add***

  添加代理。**-n**代理名字，**-a**监听地址，**-p**监听端口，**-m**该代理的最大连接数，**-i**JSON格式的input配置，**-o**JSON格式的output配置

  `add -n l1 -a 127.0.0.1 -p 12345 -i '{"protocol":"socks5"}' -o '{"protocol":"shadowsocks", "serverAddr":"example.com","serverPort":12222,"method":"aes-256-gcm","password":"aaaa"}'`

* ***remove***

  删除代理，代理必须已停止才能删除。

  `remove l1 l2`

* ***start***

  启动代理

  `start l1 l2`

* ***stop***

  停止代理

  `stop l1 l2`

* ***update***

  更新代理配置，参数与add一样，必须指定代理名**-n**，其他可选。

  `update -n l1 -m 123`

* ***stat***

  获取代理的流量统计，默认按照小时统计，添加参数**-d**按照天统计。

  `stat -d l1`

### 截图
![安装运行][1]

### 额外说明
* 当前并不支持UDP代理，以后也不打算支持。


  [1]: https://gitlab.com/wiky.lyu/galaxy/raw/master/screenshot/install&run.png
