###Direct

***没有配置选项***

###Gastly
* serverAddr 服务器地址
* serverPort 服务器端口号
* username 用户名
* password 密码
* format 数据格式
* insecure 是否允许不安全链接

###Shadowsocks
* serverAddr 服务器地址
* serverPort 服务器端口号
* method 加密方式
* password 密码

###Socks5
* serverAddr 服务器地址
* serverPort 服务器端口号
* username 用户名
* password 密码
