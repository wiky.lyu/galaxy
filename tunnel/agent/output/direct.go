/*
 * Copyright (C) 2018-2020 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */
package output

import (
	"fmt"
	"gitlab.com/wiky.lyu/galaxy/gnet"
	"gitlab.com/wiky.lyu/galaxy/tunnel/agent"
)

type DirectFactory struct {
}

func (*DirectFactory) Name() string {
	return "direct"
}

func (*DirectFactory) Options() map[string]string {
	return map[string]string{}
}

func (f *DirectFactory) Setup(agent.Options) error {
	return nil
}

func (f *DirectFactory) NewOAgent(addr string, port uint16) (agent.OAgent, error) {
	gc, err := gnet.Dial("tcp", fmt.Sprintf("%s:%d", addr, port))
	if err != nil {
		return nil, err
	}
	return gc, nil
}
