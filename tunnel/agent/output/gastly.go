/*
 * Copyright (C) 2019-2020 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */

package output

import (
	"crypto/tls"
	"errors"
	"fmt"

	"gitlab.com/wiky.lyu/galaxy/protocol/gastly"

	"gitlab.com/wiky.lyu/galaxy/tunnel/agent"
)

type GastlyFactory struct {
	serverAddr string
	serverPort uint16
	username   string
	password   string
	format     string
	insecure   bool
}

func (*GastlyFactory) Name() string {
	return "gastly"
}

func (f *GastlyFactory) Setup(options agent.Options) error {
	f.serverAddr = options.GetString("serverAddr")
	f.serverPort = uint16(options.GetInt("serverPort"))
	f.username = options.GetString("username")
	f.password = options.GetString("password")
	f.format = options.GetString("format")
	f.insecure = options.GetBool("insecure")

	if f.serverAddr == "" {
		return errors.New("invalid serverAddr")
	} else if f.serverPort == 0 {
		return errors.New("invalid serverPort")
	}

	return nil
}

func (f *GastlyFactory) Options() map[string]string {
	return map[string]string{
		"username":   f.username,
		"password":   f.password,
		"serverAddr": f.serverAddr,
		"serverPort": fmt.Sprintf("%d", f.serverPort),
		"format":     f.format,
		"insecure":   fmt.Sprintf("%v", f.insecure),
	}
}

func (f *GastlyFactory) NewOAgent(addr string, port uint16) (agent.OAgent, error) {

	gc, err := tls.Dial("tcp", fmt.Sprintf("%s:%d", f.serverAddr, f.serverPort), &tls.Config{
		InsecureSkipVerify: f.insecure,
	})
	if err != nil {
		return nil, err
	}
	a := &GastlyAgent{
		GConn:      gc,
		targetAddr: addr,
		targetPort: port,
		username:   f.username,
		password:   f.password,
		status:     gastly.ResponseStatusUnknown,
		format:     f.format,
	}
	if err := a.Start(); err != nil {
		a.Close()
		return nil, err
	}
	return a, nil
}

type GastlyAgent struct {
	GConn      *tls.Conn
	targetAddr string
	targetPort uint16
	username   string
	password   string
	format     string

	status int
}

func (a *GastlyAgent) Start() error {
	req := gastly.NewRequest(a.targetAddr, a.targetPort, a.username, a.password)
	if err := a.Write(req.Build(a.format)); err != nil {
		return err
	}
	return nil
}

func (a *GastlyAgent) Write(buf []byte) error {
	_, err := a.GConn.Write(buf)
	return err
}

func (a *GastlyAgent) Read() ([]byte, error) {
	if a.status == gastly.ResponseStatusUnknown {
		sizebuf := make([]byte, 2)
		if _, err := a.GConn.Read(sizebuf); err != nil {
			return nil, err
		}
		size := gastly.BytesToUint16(sizebuf)
		if size <= 0 || size >= 4096 {
			return nil, errors.New("invalid message")
		}
		buf := make([]byte, size)
		if n, err := a.GConn.Read(buf); err != nil || n != int(size) {
			return nil, errors.New("invalid message")
		}
		resp, err := gastly.ParseResponse(a.format, buf)
		if err != nil {
			return nil, err
		}
		if resp.Status != gastly.ResponseStatusOK {
			return nil, errors.New("connection failed")
		}
		a.status = gastly.ResponseStatusOK
	}
	buf := make([]byte, 4096)
	if n, err := a.GConn.Read(buf); err != nil {
		return nil, err
	} else {
		return buf[:n], nil
	}
}

func (a *GastlyAgent) Close() error {
	return a.GConn.Close()
}
