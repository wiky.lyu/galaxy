/*
 * Copyright (C) 2018-2020 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */

package output

import (
	"encoding/binary"
	"errors"
	"fmt"
	"log"

	"gitlab.com/wiky.lyu/galaxy/cipher"
	"gitlab.com/wiky.lyu/galaxy/gnet"
	"gitlab.com/wiky.lyu/galaxy/protocol/socks"
	"gitlab.com/wiky.lyu/galaxy/protocol/ss"
	"gitlab.com/wiky.lyu/galaxy/tunnel/agent"
	"gitlab.com/wiky.lyu/galaxy/tunnel/agent/util"
)

type ShadowsocksFactory struct {
	serverAddr string
	serverPort uint16
	method     string
	password   string
	cipherInfo *cipher.CipherInfo
}

func (*ShadowsocksFactory) Name() string {
	return "shadowsocks"
}

func (f *ShadowsocksFactory) Setup(options agent.Options) error {
	f.serverAddr = options.GetString("serverAddr")
	f.serverPort = uint16(options.GetInt("serverPort"))
	f.method = options.GetString("method")
	f.password = options.GetString("password")
	f.cipherInfo = cipher.GetCipherInfo(f.method)

	if f.serverAddr == "" {
		return errors.New("invalid serverAddr")
	} else if f.serverPort == 0 {
		return errors.New("invalid serverPort")
	} else if f.cipherInfo == nil {
		return errors.New("invalid method")
	} else if f.password == "" {
		return errors.New("invalid password")
	}

	return nil
}

func (f *ShadowsocksFactory) Options() map[string]string {
	return map[string]string{
		"method":     f.method,
		"password":   f.password,
		"serverAddr": f.serverAddr,
		"serverPort": fmt.Sprintf("%d", f.serverPort),
	}
}

func (f *ShadowsocksFactory) NewOAgent(addr string, port uint16) (agent.OAgent, error) {
	iv := cipher.RandKey(f.cipherInfo.IvSize)
	key := ss.KDF(f.password, f.cipherInfo.KeySize)
	subkey := ss.HKDFSHA1(key, f.cipherInfo.KeySize, iv)

	ekey := key
	if f.cipherInfo.Block {
		ekey = subkey
	}
	encrypter, err := f.cipherInfo.EncrypterFunc(ekey, iv)
	if err != nil {
		log.Printf("failed to create encrypter:%v", err)
		return nil, err
	}
	gc, err := gnet.Dial("tcp", fmt.Sprintf("%s:%d", f.serverAddr, f.serverPort))
	if err != nil {
		return nil, err
	}
	a := &ShadowsocksAgent{
		GConn:      gc,
		targetAddr: addr,
		targetPort: port,
		method:     f.method,
		password:   f.password,
		cipherInfo: f.cipherInfo,
		encrypter:  encrypter,
		decrypter:  nil,
		key:        key,
		subkey:     subkey,
		iv:         iv,
	}
	if err := a.Start(); err != nil {
		a.Close()
		return nil, err
	}
	return a, nil
}

type ShadowsocksAgent struct {
	*gnet.GConn
	targetAddr string
	targetPort uint16
	method     string
	password   string
	cipherInfo *cipher.CipherInfo
	encrypter  cipher.Encrypter
	decrypter  cipher.Decrypter
	key        []byte
	subkey     []byte
	iv         []byte
}

func (a *ShadowsocksAgent) Start() error {
	atype := socks.GetAddrAType(a.targetAddr)
	req := ss.NewAddressRequest(atype, a.targetAddr, a.targetPort)
	if len(a.iv) != 0 {
		if err := a.GConn.Write(a.iv); err != nil {
			return err
		}
	}
	if err := a.Write(req.Build()); err != nil {
		return err
	}
	return nil
}

func (a *ShadowsocksAgent) Write(data []byte) error {
	if a.cipherInfo.Block {
		lenbuf := a.encrypter.Encrypt(util.Uint16ToBytes(uint16(len(data)), binary.BigEndian))
		a.GConn.Write(lenbuf)
	}
	cipherdata := a.encrypter.Encrypt(data)
	return a.GConn.Write(cipherdata)
}

func (a *ShadowsocksAgent) readBlock() ([]byte, error) {
	lenbuf, err := a.ReadFull(2 + a.cipherInfo.TagSize)
	if err != nil {
		return nil, err
	}
	lenbytes := a.decrypter.Decrypt(lenbuf)
	if lenbytes == nil {
		return nil, ss.ErrInvalidMessage
	}
	length := util.BytesToUint16(lenbytes, binary.BigEndian)
	if length == 0 {
		return nil, ss.ErrInvalidMessage
	}
	databuf, err := a.ReadFull(int(length) + a.cipherInfo.TagSize)
	if err != nil {
		return nil, err
	}
	return a.decrypter.Decrypt(databuf), nil
}

func (a *ShadowsocksAgent) readStream() ([]byte, error) {
	if data, err := a.GConn.Read(); err != nil {
		return nil, err
	} else {
		return a.decrypter.Decrypt(data), nil
	}
}

func (a *ShadowsocksAgent) Read() ([]byte, error) {
	if a.decrypter == nil {
		ivbuf, err := a.GConn.ReadFull(a.cipherInfo.IvSize)
		if err != nil {
			return nil, err
		}
		ekey := a.key
		if a.cipherInfo.Block {
			ekey = ss.HKDFSHA1(a.key, a.cipherInfo.KeySize, ivbuf)
		}
		decrypter, err := a.cipherInfo.DecrypterFunc(ekey, ivbuf)
		if err != nil {
			log.Printf("failed to create decrypter:%v", err)
			return nil, err
		}
		a.decrypter = decrypter
	}
	if a.cipherInfo.Block {
		return a.readBlock()
	} else {
		return a.readStream()
	}
}
