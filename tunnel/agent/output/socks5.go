/*
 * Copyright (C) 2018-2020 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */

package output

import (
	"errors"
	"fmt"

	"gitlab.com/wiky.lyu/galaxy/gnet"
	"gitlab.com/wiky.lyu/galaxy/protocol/socks"
	"gitlab.com/wiky.lyu/galaxy/tunnel/agent"
)

type Socks5Factory struct {
	username   string
	password   string
	serverAddr string
	serverPort uint16
}

func (*Socks5Factory) Name() string {
	return "socks5"
}
func (f *Socks5Factory) Setup(options agent.Options) error {
	f.serverAddr = options.GetString("serverAddr")
	f.serverPort = uint16(options.GetInt("serverPort"))
	f.username = options.GetString("username")
	f.password = options.GetString("password")
	if f.serverAddr == "" {
		return errors.New("invalid serverAddr")
	} else if f.serverPort == 0 {
		return errors.New("invalid serverPort")
	}
	return nil
}

func (f *Socks5Factory) Options() map[string]string {
	m := make(map[string]string)
	if f.username != "" {
		m["username"] = f.username
	}
	if f.password != "" {
		m["password"] = f.password
	}
	return m
}

func (f *Socks5Factory) NewOAgent(addr string, port uint16) (agent.OAgent, error) {
	gc, err := gnet.Dial("tcp", fmt.Sprintf("%s:%d", f.serverAddr, f.serverPort))
	if err != nil {
		return nil, err
	}
	a := &Socks5Agent{
		GConn:      gc,
		username:   f.username,
		password:   f.password,
		targetAddr: addr,
		targetPort: port,
	}
	if err := a.Start(); err != nil {
		a.Close()
		return nil, err
	}
	return a, nil
}

type Socks5Agent struct {
	*gnet.GConn
	username   string
	password   string
	targetAddr string
	targetPort uint16
	buf        []byte
}

func (a *Socks5Agent) doMethodSelection() (byte, error) {
	method := socks.MethodNoAuthRequired
	if a.username != "" && a.password != "" {
		method = socks.MethodUsernamePassword
	}
	req := socks.NewMethodSelectionRequest(socks.Version5, method)
	if err := a.GConn.Write(req.Build()); err != nil {
		return 0, err
	}

	buf, err := a.GConn.Read()
	if err != nil {
		return 0, err
	}
	rep, err := socks.ParseMethodSelectionReply(buf)
	if err != nil {
		return 0, err
	} else if rep.METHOD == socks.MethodNoAcceptable {
		return 0, fmt.Errorf("No Acceptable Method")
	}
	return rep.METHOD, nil
}

func (a *Socks5Agent) doUsernamePassword() error {
	req := socks.NewUsernamePasswordRequest(socks.Version5, a.username, a.password)
	if err := a.GConn.Write(req.Build()); err != nil {
		return err
	}
	buf, err := a.GConn.Read()
	if err != nil {
		return err
	}
	rep, err := socks.ParseUsernamePasswordReply(buf)
	if err != nil {
		return err
	}
	if rep.STATUS != socks.UsernamePasswordStatusSuccess {
		return fmt.Errorf("Invalid Username/Password")
	}
	return nil
}

func (a *Socks5Agent) doCMDRequest() error {
	atype := socks.GetAddrAType(a.targetAddr)
	req := socks.NewSocks5Request(socks.Version5, socks.CMDConnect, atype, a.targetAddr, a.targetPort)
	if err := a.GConn.Write(req.Build()); err != nil {
		return err
	}
	buf, err := a.GConn.Read()
	if err != nil {
		return err
	}
	rep, err := socks.ParseSocks5Reply(buf)
	if err != nil {
		return err
	} else if rep.REP != socks.ReplySuccess {
		return fmt.Errorf("Connection Failure")
	}
	a.buf = rep.BUF
	return nil
}

func (a *Socks5Agent) Start() error {
	if method, err := a.doMethodSelection(); err != nil {
		return err
	} else if method == socks.MethodUsernamePassword {
		if err := a.doUsernamePassword(); err != nil {
			return err
		}
	}
	return a.doCMDRequest()
}

func (a *Socks5Agent) Read() ([]byte, error) {
	if len(a.buf) > 0 {
		buf := a.buf
		a.buf = nil
		return buf, nil
	}
	return a.GConn.Read()
}
