/*
 * Copyright (C) 2018-2020 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */
package agent

import "gitlab.com/wiky.lyu/galaxy/gnet"

type IAgentFactory interface {
	Name() string
	Setup(Options) error
	Options() map[string]string
	NewIAgent(*gnet.GConn) (IAgent, error)
}

type IAgent interface {
	Init() (string, uint16, error)
	Notify(error) error
	Read() ([]byte, error)
	Write([]byte) error
	Close() error
}

type OAgentFactory interface {
	Name() string
	Setup(Options) error
	Options() map[string]string
	NewOAgent(string, uint16) (OAgent, error)
}

type OAgent interface {
	Read() ([]byte, error)
	Write([]byte) error
	Close() error
}
