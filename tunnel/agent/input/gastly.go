/*
 * Copyright (C) 2020 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */

package input

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/wiky.lyu/galaxy/gnet"
	"gitlab.com/wiky.lyu/galaxy/protocol/gastly"
	"gitlab.com/wiky.lyu/galaxy/tunnel/agent"
	"gitlab.com/wiky.lyu/galaxy/tunnel/agent/input/storage/mysql"
	"gitlab.com/wiky.lyu/galaxy/tunnel/agent/input/storage/none"
	"gitlab.com/wiky.lyu/galaxy/tunnel/agent/input/storage/psql"
	"gitlab.com/wiky.lyu/galaxy/tunnel/agent/input/storage/sqlite"
)

type Storage interface {
	Auth(string, string) bool
}

const (
	StorageTypeNone   = "none"
	StorageTypePSQL   = "psql"
	StorageTypeMySQL  = "mysql"
	StorageTypeSQLite = "sqlite"
)

type GastlyFactory struct {
	format string

	crt         string
	key         string
	storageType string
	storageAddr string

	fakeHost string

	cer     tls.Certificate
	storage Storage
}

func (*GastlyFactory) Name() string {
	return "gastly"
}

func (f *GastlyFactory) Setup(options agent.Options) error {
	f.format = options.GetString("format")
	f.fakeHost = options.GetString("fakeHost")

	f.crt = options.GetString("crt")
	f.key = options.GetString("key")
	f.storageType = options.GetString("storageType")
	f.storageAddr = options.GetString("storageAddr")

	cer, err := tls.LoadX509KeyPair(f.crt, f.key)
	if err != nil {
		return err
	}
	f.cer = cer

	switch f.storageType {
	case StorageTypePSQL:
		f.storage = psql.New(f.storageAddr)
	case StorageTypeMySQL:
		f.storage = mysql.New(f.storageAddr)
	case StorageTypeSQLite:
		f.storage = sqlite.New(f.storageAddr)
	case StorageTypeNone:
		f.storage = none.New()
	default:
		return fmt.Errorf("invalid storage type: %s", f.storageAddr)
	}
	return nil
}

func (f *GastlyFactory) Options() map[string]string {
	return map[string]string{
		"format":       f.format,
		"fakeHost":    f.fakeHost,
		"crt":          f.crt,
		"key":          f.key,
		"storageType": f.storageType,
		"storageAddr": f.storageAddr,
	}
}

type GastlyAgent struct {
	*gnet.GConn

	tlsConn *tls.Conn

	format   string
	fakeHost string

	cer     tls.Certificate
	storage Storage
}

func (f *GastlyFactory) NewIAgent(gc *gnet.GConn) (agent.IAgent, error) {
	return &GastlyAgent{
		GConn: gc,

		format:   f.format,
		fakeHost: f.fakeHost,
		cer:      f.cer,
		storage:  f.storage,
	}, nil
}

func (a *GastlyAgent) Init() (string, uint16, error) {
	a.tlsConn = tls.Server(a.Conn, &tls.Config{
		Certificates: []tls.Certificate{a.cer},
	})

	if err := a.tlsConn.Handshake(); err != nil {
		return "", 0, err
	}

	buf := make([]byte, 4096)
	n, err := a.tlsConn.Read(buf)
	if err != nil {
		return "", 0, err
	} else if n <= 2 {
		return "", 0, fmt.Errorf("invalid message")
	}
	buf = buf[:n]

	if bytes.HasPrefix(buf, []byte(http.MethodGet)) || bytes.HasPrefix(buf, []byte(http.MethodPost)) || bytes.HasPrefix(buf, []byte(http.MethodPut)) ||
		bytes.HasPrefix(buf, []byte(http.MethodDelete)) || bytes.HasPrefix(buf, []byte(http.MethodHead)) || bytes.HasPrefix(buf, []byte(http.MethodPatch)) ||
		bytes.HasPrefix(buf, []byte(http.MethodOptions)) || bytes.HasPrefix(buf, []byte(http.MethodTrace)) || bytes.HasPrefix(buf, []byte(http.MethodConnect)) {
		if a.fakeHost != "" {
			resp, err := http.Get(a.fakeHost)
			if err != nil {
				return "", 0, err
			}
			defer resp.Body.Close()
			go ioutil.ReadAll(a.tlsConn)
			resp.Write(a.tlsConn)
		} else {
			fakeHTTP := []byte("HTTP/1.1 200 OK\r\nServer: NMSL 2.0\r\nConnection: closed\r\n\r\n")
			a.tlsConn.Write(fakeHTTP)
		}
		return "", 0, nil
	}

	/* 握手包大小不能超过4096 */
	size := gastly.BytesToUint16(buf[:2])
	if size <= 0 || size >= 4094 || uint16(n-2) > size {
		return "", 0, fmt.Errorf("invalid message")
	}
	buf = buf[2:]
	if uint16(n-2) < size {
		left := size - uint16(n-2)
		leftbuf := make([]byte, left)
		if n, err := a.tlsConn.Read(leftbuf); err != nil || n != int(left) {
			return "", 0, fmt.Errorf("invalid message")
		}
		buf = append(buf, leftbuf...)
	}

	req, err := gastly.ParseRequest(a.format, buf)
	if err != nil {
		return "", 0, err
	}

	if !a.storage.Auth(req.Username, req.Password) {
		/* 认证失败 */
		return "", 0, fmt.Errorf("auth failed %s:%s", req.Username, req.Password)
	}
	return req.Addr, req.Port, nil
}

func (a *GastlyAgent) Notify(err error) error {
	if err == nil {
		resp := gastly.NewResponse(gastly.ResponseStatusOK)
		if _, err := a.tlsConn.Write(resp.Build(a.format)); err != nil {
			return err
		}
	}
	return err
}

func (a *GastlyAgent) Read() ([]byte, error) {
	buf := make([]byte, 4096)
	n, err := a.tlsConn.Read(buf)
	if err != nil {
		return nil, err
	}
	return buf[:n], nil
}

func (a *GastlyAgent) Write(data []byte) error {
	_, err := a.tlsConn.Write(data)
	return err
}

func (a *GastlyAgent) Close() error {
	if a.tlsConn != nil {
		return a.tlsConn.Close()
	}
	return a.Conn.Close()
}
