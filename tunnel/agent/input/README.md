###HTTP(S)

* username 用户名(nullable)
* password 密码(nullable)

###Gastly

* format 数据格式(json/bson/xml/gob)，默认为bson
* fakeHost 虚拟域名(nullable)
* crt
* key TLS密钥
* storageType 用户认证方式(mysql/psql/none/sqlite)
* storageAddr 对应storage_type的地址，如/usr/local/share/gastly.sqlite

###Shadowsocks

* method 加密方式
* password 密码

###Socks5

* username 用户名(nullable)
* password 密码(nullable)
