/*
 * Copyright (C) 2018-2020 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */
package mysql

import (
	"database/sql"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

type MySQL struct {
	db *sql.DB
}

func New(url string) *MySQL {
	db, err := sql.Open("mysql", url)
	if err != nil {
		log.Fatalln(err)
	}
	log.Printf("MySQL enabled: %v", url)
	return &MySQL{db: db}
}

func (p *MySQL) Auth(username, password string) bool {
	r, err := p.db.Query(`SELECT 1 FROM gastly_auth WHERE username=? AND password=?`, username, password)
	if err != nil {
		log.Printf("SQL Error: %v", err)
		return false
	}
	defer r.Close()
	return r.Next()
}
