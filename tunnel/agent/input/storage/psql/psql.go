/*
 * Copyright (C) 2018-2020 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */
package psql

import (
	"log"

	"github.com/go-pg/pg"
)

type PSQL struct {
	db *pg.DB
}

func New(url string) *PSQL {
	options, err := pg.ParseURL(url)
	if err != nil {
		log.Fatalln(err)
	}
	db := pg.Connect(options)
	log.Printf("PSQL enabled: %v", url)
	return &PSQL{db: db}
}

func (p *PSQL) Auth(username, password string) bool {
	r, err := p.db.Query(`SELECT 1 FROM gastly_auth WHERE username=? AND password=?`, username, password)
	if err != nil {
		log.Printf("SQL Error: %v", err)
		return false
	}
	return r.RowsReturned() > 0
}
