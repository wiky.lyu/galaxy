/*
 * Copyright (C) 2018-2020 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */
package tunnel

// Error Tunnel相关的错误
type Error struct {
	Status  uint32
	Message string
}

func (e *Error) Error() string {
	return e.Message
}

func NewError(code uint32, message string) *Error {
	return &Error{
		Status:  code,
		Message: message,
	}
}

const (
	StatusOK = 0

	StatusInputProtocolUnknown        = 1
	StatusOutputProtocolUnknown       = 2
	StatusInputProtocolOptionInvalid  = 3
	StatusOutputProtocolOptionInvalid = 4
	StatusAddrInvalid                 = 5
	StatusPortInvalid                 = 6

	StatusTunnelNotFound       = 7
	StatusTunnelNameExists     = 8
	StatusTunnelAlreadyRunning = 9
	StatusTunnelNotRunning     = 10
	StatusTunnelRunning        = 11
	StatusTunnelListenFailure  = 12
	StatusTunnelNameInvalid    = 13
)

var (
	Status_name = map[int]string{
		StatusOK:                          "OK",
		StatusInputProtocolUnknown:        "unknown input protocol",
		StatusOutputProtocolUnknown:       "unknown output protocol",
		StatusInputProtocolOptionInvalid:  "invalid input protocol options",
		StatusOutputProtocolOptionInvalid: "invalid output protocol options",
		StatusAddrInvalid:                 "invalid addr",
		StatusPortInvalid:                 "invalid port",
		StatusTunnelNotFound:              "tunnel not found",
		StatusTunnelNameExists:            "name already exists",
		StatusTunnelAlreadyRunning:        "tunnel already running",
		StatusTunnelNotRunning:            "tunnle isn't running",
		StatusTunnelRunning:               "tunnel is running",
		StatusTunnelListenFailure:         "listen failure",
		StatusTunnelNameInvalid:           "invalid tunnel name",
	}
)

func GetStatusName(status uint32) string {
	name := Status_name[int(status)]
	if name == "" {
		name = "unknown error"
	}
	return name
}

var (
	ErrInputProtocolUnknown        = NewError(StatusInputProtocolUnknown, "unknown input protocol")
	ErrOutputProtocolUnknown       = NewError(StatusOutputProtocolUnknown, "unknown output protocol")
	ErrInputProtocolOptionInvalid  = NewError(StatusInputProtocolOptionInvalid, "invalid options")
	ErrOutputProtocolOptionInvalid = NewError(StatusOutputProtocolOptionInvalid, "invalid options")
	ErrAddrInvalid                 = NewError(StatusAddrInvalid, "invalid addr")
	ErrPortInvalid                 = NewError(StatusPortInvalid, "invalid port")

	ErrTunnelAlreadyRunning = NewError(StatusTunnelAlreadyRunning, "already running")
	ErrTunnelNotRunning     = NewError(StatusTunnelNotRunning, "not running")
	ErrTunnelNotFound       = NewError(StatusTunnelNotFound, "tunnel not found")
	ErrTunnelNameExists     = NewError(StatusTunnelNameExists, "tunnel name exists")
	ErrTunnelRunning        = NewError(StatusTunnelRunning, "tunnel is running")
	ErrTunnelListenFailure  = NewError(StatusTunnelListenFailure, "listen failure")
	ErrTunnelNameInvalid    = NewError(StatusTunnelNameInvalid, "invalid tunnel name")
)
