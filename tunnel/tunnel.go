/*
 * Copyright (C) 2018-2020 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */

package tunnel

import (
	"fmt"
	"log"
	"net"
	"sync"
	"sync/atomic"
	"time"

	"gitlab.com/wiky.lyu/galaxy/gnet"
	"gitlab.com/wiky.lyu/galaxy/tunnel/agent"
	"gitlab.com/wiky.lyu/galaxy/tunnel/agent/input"
	"gitlab.com/wiky.lyu/galaxy/tunnel/agent/output"
)

type NewIAgentFactoryFunc func() agent.IAgentFactory
type NewOAgentFactoryFunc func() agent.OAgentFactory

/* INPUT */
func NewSocks5IAgentFactory() agent.IAgentFactory {
	return &input.Socks5Factory{}
}

func NewShadowsocksIAgentFactory() agent.IAgentFactory {
	return &input.ShadowsocksFactory{}
}

func NewHttpIAgentFactory() agent.IAgentFactory {
	return &input.HttpFactory{}
}

/* OUTPUT */
func NewDirectOAgentFactory() agent.OAgentFactory {
	return &output.DirectFactory{}
}

func NewShadowsocksOAgentFactory() agent.OAgentFactory {
	return &output.ShadowsocksFactory{}
}

func NewSocks5OAgentFactory() agent.OAgentFactory {
	return &output.Socks5Factory{}
}

func NewGastlyOAgentFactory() agent.OAgentFactory {
	return &output.GastlyFactory{}
}

func NewGastlyIAgentFactory() agent.IAgentFactory {
	return &input.GastlyFactory{}
}

var (
	inputFactoryFuncs = map[string]NewIAgentFactoryFunc{
		"socks5":      NewSocks5IAgentFactory,
		"shadowsocks": NewShadowsocksIAgentFactory,
		"http":        NewHttpIAgentFactory,
		"gastly":      NewGastlyIAgentFactory,
	}
	outputFactoryFuncs = map[string]NewOAgentFactoryFunc{
		"direct":      NewDirectOAgentFactory,
		"socks5":      NewSocks5OAgentFactory,
		"shadowsocks": NewShadowsocksOAgentFactory,
		"gastly":      NewGastlyOAgentFactory,
	}
)

const (
	StatusIdle    = 0
	StatusRunning = 1
	StatusError   = 2
)

type Link struct {
	IAddr string
	OAddr string
}

type Tunnel struct {
	sync.Mutex
	Name      string
	Addr      string
	Port      uint16
	IFactory  agent.IAgentFactory
	OFactory  agent.OAgentFactory
	Status    int
	FlowStats []*FlowStat
	signal    chan bool

	CurConn int32
	MaxConn int32
	STime   time.Time // 开始时间
}

// New 创建一个新的代理，(addr,port)为监听端口，iOptions和 oOptions分别为input和output的协议
func New(addr string, port uint16, iOptions, oOptions agent.Options, maxConn int32) (*Tunnel, *Error) {
	iName := iOptions.GetString("protocol")
	oName := oOptions.GetString("protocol")
	iFactoryFunc := inputFactoryFuncs[iName]
	oFactoryFunc := outputFactoryFuncs[oName]
	if iFactoryFunc == nil {
		return nil, ErrInputProtocolUnknown
	} else if oFactoryFunc == nil {
		return nil, ErrOutputProtocolUnknown
	}
	iFactory := iFactoryFunc()
	oFactory := oFactoryFunc()

	if err := iFactory.Setup(iOptions); err != nil {
		return nil, ErrInputProtocolOptionInvalid
	} else if err := oFactory.Setup(oOptions); err != nil {
		return nil, ErrOutputProtocolOptionInvalid
	} else if port == 0 {
		return nil, ErrPortInvalid
	}

	return &Tunnel{
		Addr:      addr,
		Port:      port,
		IFactory:  iFactory,
		OFactory:  oFactory,
		FlowStats: make([]*FlowStat, 0),
		Status:    StatusIdle,
		MaxConn:   maxConn,
		CurConn:   0,
	}, nil
}

func (t *Tunnel) Update(addr string, port uint16, iOptions, oOptions agent.Options, maxConn int32) *Error {
	defer t.Unlock()
	t.Lock()
	var iFactory agent.IAgentFactory
	var oFactory agent.OAgentFactory
	if iOptions != nil {
		iName := iOptions.GetString("protocol")
		iFactoryFunc := inputFactoryFuncs[iName]
		if iFactoryFunc == nil {
			return ErrInputProtocolUnknown
		}
		iFactory = iFactoryFunc()
		if err := iFactory.Setup(iOptions); err != nil {
			return ErrInputProtocolOptionInvalid
		}
	}
	if oOptions != nil {
		oName := oOptions.GetString("protocol")
		oFactoryFunc := outputFactoryFuncs[oName]
		if oFactoryFunc == nil {
			return ErrOutputProtocolUnknown
		}
		oFactory = oFactoryFunc()
		if err := oFactory.Setup(oOptions); err != nil {
			return ErrOutputProtocolOptionInvalid
		}
	}
	if addr != "" {
		t.Addr = addr
	}
	if port > 0 {
		t.Port = port
	}
	if iFactory != nil {
		t.IFactory = iFactory
	}
	if oFactory != nil {
		t.OFactory = oFactory
	}
	if maxConn != 0 {
		t.MaxConn = maxConn
	}
	return nil
}

// Stop 关闭代理
func (t *Tunnel) Stop() *Error {
	defer t.Unlock()
	t.Lock()
	if t.Status != StatusRunning {
		return ErrTunnelNotRunning
	}
	t.signal <- true
	<-t.signal
	t.signal = nil
	return nil
}

// Start 启动代理
func (t *Tunnel) Start() *Error {
	defer t.Unlock()
	t.Lock()
	if t.Status == StatusRunning {
		return ErrTunnelAlreadyRunning
	}
	listener, err := gnet.Listen("tcp", fmt.Sprintf("%s:%d", t.Addr, t.Port))
	if err != nil {
		return ErrTunnelListenFailure
	}
	t.Status = StatusRunning
	t.STime = time.Now()
	t.signal = make(chan bool, 1)
	go t.run(listener)
	return nil
}

func (t *Tunnel) run(listener *gnet.GListener) {
	defer listener.Close()

	cc := make(chan *gnet.GConn, 128)
	go func() {
		defer close(cc)
		for {
			if c, err := listener.Accept(); err != nil {
				opErr := err.(*net.OpError)
				if opErr != nil && opErr.Err.Error() == "use of closed network connection" {
					break
				} else {
					log.Println(err)
				}
			} else {
				cc <- c
			}
		}
	}()
LOOP:
	for {
		select {
		case quit, _ := <-t.signal:
			if quit {
				break LOOP
			}
		case c, ok := <-cc:
			if !ok {
				break LOOP
			}
			go t.runIO(c)
		}
	}

	t.Status = StatusIdle
	close(t.signal)
}

func (t *Tunnel) runIO(c *gnet.GConn) {
	if t.MaxConn > 0 && t.MaxConn <= t.CurConn {
		log.Printf("%s - too many connections", t.Name)
		c.Close()
		return
	}

	atomic.AddInt32(&t.CurConn, 1)
	defer atomic.AddInt32(&t.CurConn, -1)

	iAgent, err := t.IFactory.NewIAgent(c)
	if err != nil {
		c.Close()
		return
	}
	defer iAgent.Close()
	addr, port, err := iAgent.Init()
	if err != nil || addr == "" || port <= 0 {
		log.Printf("%s - iAgent.Init() failed: %v", t.Name, err)
		return
	}
	oAgent, err := t.OFactory.NewOAgent(addr, port)
	if err != nil {
		log.Printf("%s - NewOAgent() failed: %v", t.Name, err)
		iAgent.Notify(err)
		return
	}
	defer oAgent.Close()
	if err := iAgent.Notify(nil); err != nil {
		return
	}

	// log.Printf("[%s] %s <==> %s:%d connected\n", t.Name, c.RemoteAddr().String(), addr, port)
	// defer log.Printf("[%s] %s <==> %s:%d closed\n", t.Name, c.RemoteAddr().String(), addr, port)
	c1 := gnet.ConnChannel(iAgent)
	c2 := gnet.ConnChannel(oAgent)

	/* 30秒更新一次流量统计 */
	var ingress, egress uint64
	ticker := time.NewTicker(30 * time.Second)
LOOP:
	for {
		select {
		case data, ok := <-c1:
			if !ok {
				break LOOP
			} else if err := oAgent.Write(data); err != nil {
				break LOOP
			}
			ingress += uint64(len(data))
		case data, ok := <-c2:
			if !ok {
				break LOOP
			} else if err := iAgent.Write(data); err != nil {
				break LOOP
			}
			egress += uint64(len(data))
		case <-ticker.C:
			t.IE(ingress, egress)
			ingress = 0
			egress = 0
		}
	}
	t.IE(ingress, egress)
}
