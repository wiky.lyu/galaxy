/*
 * Copyright (C) 2019-2020 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */

package gastly

import (
	"bytes"
	"encoding/binary"
	"fmt"
)

type Request struct {
	Addr     string `json:"addr" bson:"addr" xml:"addr"`
	Port     uint16 `json:"port" bson:"port" xml:"port"`
	Username string `json:"username" bson:"username" xml:"username"`
	Password string `json:"password" bson:"password" xml:"password"`
}

func (r *Request) Address() string {
	return fmt.Sprintf("%s:%d", r.Addr, r.Port)
}

func ParseRequest(format string, buf []byte) (*Request, error) {
	var req Request
	if err := formatUnmarshal(format, buf, &req); err != nil {
		return nil, err
	}
	return &req, nil
}

func NewRequest(addr string, port uint16, username, password string) *Request {
	return &Request{
		Addr:     addr,
		Port:     port,
		Username: username,
		Password: password,
	}
}

func (r *Request) Build(format string) []byte {
	buf, err := formatMarshal(format, r)
	if err != nil {
		return nil
	}
	sizebuf := Uint16ToBytes(uint16(len(buf)))
	var buffer bytes.Buffer
	buffer.Write(sizebuf)
	buffer.Write(buf)

	return buffer.Bytes()
}

const (
	ResponseStatusUnknown = -1
	ResponseStatusOK      = 0
	ResponseStatusFailed  = 1
)

type Response struct {
	Status int `json:"status" bson:"status" xml:"status"`
}

func ParseResponse(format string, buf []byte) (*Response, error) {
	var resp Response
	if err := formatUnmarshal(format, buf, &resp); err != nil {
		return nil, err
	}
	return &resp, nil
}

func NewResponse(status int) *Response {
	return &Response{
		Status: status,
	}
}

func (r *Response) Build(format string) []byte {
	buf, err := formatMarshal(format, r)
	if err != nil {
		return nil
	}
	sizebuf := Uint16ToBytes(uint16(len(buf)))
	var buffer bytes.Buffer
	buffer.Write(sizebuf)
	buffer.Write(buf)

	return buffer.Bytes()
}

func BytesToUint16(b []byte) uint16 {
	var i uint16
	buf := bytes.NewReader(b)
	if err := binary.Read(buf, binary.BigEndian, &i); err != nil {
		return 0
	}
	return i
}

func Uint16ToBytes(i uint16) []byte {
	w := bytes.NewBuffer(nil)
	if err := binary.Write(w, binary.BigEndian, i); err != nil {
		return nil
	}
	return w.Bytes()
}
