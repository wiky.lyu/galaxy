/*
 * Copyright (C) 2018-2020 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */
package gastly

import (
	"bytes"
	"encoding/gob"
	"encoding/json"
	"encoding/xml"

	"go.mongodb.org/mongo-driver/bson"
)

const (
	FormatJSON = "json"
	FormatGOB  = "gob"
	FormatXML  = "xml"
	FormatBSON = "bson"
)

func formatMarshal(format string, r interface{}) ([]byte, error) {
	var buf []byte
	var err error

	switch format {
	case FormatJSON:
		buf, err = json.Marshal(r)
	case FormatGOB:
		var buffer bytes.Buffer
		enc := gob.NewEncoder(&buffer)
		err = enc.Encode(r)
		buf = buffer.Bytes()
	case FormatXML:
		buf, err = xml.Marshal(r)
	default:
		buf, err = bson.Marshal(r)
	}
	return buf, err
}

func formatUnmarshal(format string, buf []byte, r interface{}) error {
	var err error

	switch format {
	case FormatJSON:
		err = json.Unmarshal(buf, r)
	case FormatGOB:
		buffer := bytes.NewBuffer(buf)
		dec := gob.NewDecoder(buffer)
		err = dec.Decode(r)
	case FormatXML:
		err = xml.Unmarshal(buf, r)
	default:
		err = bson.Unmarshal(buf, r)
	}

	return err
}
